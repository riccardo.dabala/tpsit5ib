# Maree

Quest'applicazione permette di visualizzare le informazioni sulle maree della Città Metropolitana di Venezia.

Questo è la classe per i cubit:

### Cubit
<pre><code> 
    class TideCubit extends Cubit<TideState> {
        final TideRepository _tideRepository;

        TideCubit(this._tideRepository) : super(TideInitial());

        Future<void> gettide() async {
            try {
                emit(TideLoading());
                final tide = await _tideRepository.fetchData();
                emit(TideLoaded(tide));
            } on NetworkException {
                emit(TideError("Couldn't fetch tide. Is the device online?"));
            }
        }
    }
</code></pre>

Queste sono le classi per la gestione degli stati:

### TideInitial
<pre><code> 
    class TideInitial extends TideState {
        const TideInitial();
    }
</code></pre>

### TideLoading
<pre><code> 
    class TideLoading extends TideState {
        const TideLoading();
    }
</code></pre>

### TideLoaded
<pre><code> 
    class TideLoaded extends TideState {
        final List<Tide> tide;
        const TideLoaded(this.tide);

        @override
        bool operator ==(Object o) {
            if (identical(this, o)) return true;

            return o is TideLoaded && o.tide == tide;
        }

        @override
        int get hashCode => tide.hashCode;
    }
</code></pre>

### TideError
<pre><code> 
    class TideError extends TideState {
        final String message;
        const TideError(this.message);

        @override
        bool operator ==(Object o) {
            if (identical(this, o)) return true;

            return o is TideError && o.message == message;
        }

        @override
        int get hashCode => message.hashCode;
    }
</code></pre>

Nel modello Tide è presente il metodo per ottenere i dati dal json disponibile sul sito https://dati.venezia.it/sites/default/files/dataset/opendata/livello.json

### Ottenimento dati
<pre><code>
    factory Tide.fromJson(Map<String, dynamic> json) => Tide(
        ordine: json['ordine'] as String,
        idStazione: json['ID_stazione'] as String,
        stazione: json['stazione'] as String,
        nomeAbbr: json['nome_abbr'] as String,
        latDMSN: json['latDMSN'] as String,
        lonDMSE: json['lonDMSE'] as String,
        latDDN: json['latDDN'] as String,
        lonDDE: json['lonDDE'] as String,
        data: json['data'] as String,
        valore: json['valore'] as String,
    );
</code></pre>

Ho cambiato il colore dello sfondo in azzurro con questa riga di codice:

### Sfondo colorato
  <pre><code>
    backgroundColor: Color(0xFF4da3ff),
  </code></pre>

Per l'interfaccia della schermata principale ho utilizzato:

- Un BlocConsumer che a seconda dello stato in cui si trova l'applicazione chiama un builder per costruire l'UI:

### BlocConsumer
<pre><code>
    BlocConsumer<TideCubit, TideState>(
        listener: (context, state) {
            if (state is TideError) {
                ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                ),
              );
            }
        },
        builder: (context, state) {
            if (state is TideInitial || state is TideLoading) {
                return buildLoading();
            } else if (state is TideLoaded) {
                return buildListViewWithData(state.tide);
            } else {
                return buildLoading();
            }
        },
    ),  
</code></pre>

- I builder che si occupano di costruire l'interfaccia:

### buildLoading()
<pre><code>
    Widget buildLoading() {
        return Center(
        child: CircularProgressIndicator(),
        );
    }
</code></pre>

### buildListViewWithData()
<pre><code>
    ListView buildListViewWithData(List<Tide> tide) {
        return ListView.builder(
            shrinkWrap: true,
            itemCount: tide.length,
            itemBuilder: (context, index) => buildItem(tide[index]),
        );
    }
</code></pre>

- Il builder che si occupa di costruire gli Item

### buildItem()
<pre><code>
Widget buildItem(Tide tide) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: Card(
            elevation: 10,
            color: Color(0xFFFFFFFF),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 30.0, bottom: 30, left: 13.0, right: 22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      tide.stazione,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 20,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                  Container(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "LAT: " + tide.latDDN + ", LON: " + tide.lonDMSE,
                      style: TextStyle(
                        fontSize: 10,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                  Container(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      tide.valore,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
</code></pre>

Esso è costituito da Card che contengono al loro interno il testo con le informazioni relative ad ogni stazione:

### Nome della stazione
<pre><code>
    Align(
        alignment: Alignment.center,
        child: Text(
            tide.stazione,
            style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 20,
                color: Color(0xFF007aff),
            ),
        ),
    ),
</code></pre>

### Latitudine e Longitudine
<pre><code>
    Align(
        alignment: Alignment.center,
        child: Text(
            "LAT: " + tide.latDDN + ", LON: " + tide.lonDMSE,
            style: TextStyle(
                fontSize: 10,
                color: Color(0xFF007aff),
            ),
        ),
    ),
</code></pre>

### Livello della marea
<pre><code>
    Align(
        alignment: Alignment.center,
        child: Text(
            tide.valore,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,
                color: Color(0xFF007aff),
            ),
        ),
    ),
</code></pre>