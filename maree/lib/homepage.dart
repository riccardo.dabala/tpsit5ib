import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maree/data/model/Tide.dart';
import 'cubit/tide_cubit.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      context.bloc<TideCubit>().gettide();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF4da3ff),
      appBar: AppBar(
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
        title: Text(
          "Maree",
          style: TextStyle(
            color: Color(0xFF0063cc),
            fontWeight: FontWeight.w600,
            fontSize: 24,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16),
        alignment: Alignment.center,
        child: BlocConsumer<TideCubit, TideState>(
          listener: (context, state) {
            if (state is TideError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is TideInitial || state is TideLoading) {
              return buildLoading();
            } else if (state is TideLoaded) {
              return buildListViewWithData(state.tide);
            } else {
              return buildLoading();
            }
          },
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  ListView buildListViewWithData(List<Tide> tide) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: tide.length,
      itemBuilder: (context, index) => buildItem(tide[index]),
    );
  }

  Widget buildItem(Tide tide) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: Card(
            elevation: 10,
            color: Color(0xFFFFFFFF),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 30.0, bottom: 30, left: 13.0, right: 22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      tide.stazione,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 20,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                  Container(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "LAT: " + tide.latDDN + ", LON: " + tide.lonDMSE,
                      style: TextStyle(
                        fontSize: 10,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                  Container(
                    height: 4,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      tide.valore,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color(0xFF007aff),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
