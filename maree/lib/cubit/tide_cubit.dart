import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:maree/data/TideRepository.dart';
import 'package:maree/data/model/Tide.dart';

part 'tide_state.dart';

class TideCubit extends Cubit<TideState> {
  final TideRepository _tideRepository;

  TideCubit(this._tideRepository) : super(TideInitial());

  Future<void> gettide() async {
    try {
      emit(TideLoading());
      final tide = await _tideRepository.fetchData();
      emit(TideLoaded(tide));
    } on NetworkException {
      emit(TideError("Couldn't fetch tide. Is the device online?"));
    }
  }
}