import 'client.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat Room',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Chat Room'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Client client = Client();
  final TextEditingController controller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF202020),
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Expanded(
                child: new ListView.builder(
                    itemCount: Client.messages.length,
                    itemBuilder: (BuildContext ctxt, int Index) {
                      Text t = new Text(Client.messages[Index],
                          style: TextStyle(color: Colors.white));
                      return t;
                    })),
            new Container(
              width: 400,
              height: 30,
              child: TextField(
                style:
                    TextStyle(fontSize: 20.0, height: 1.3, color: Colors.white),
                textAlign: TextAlign.center,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration.collapsed(
                  hintText: 'Write a message...',
                  hintStyle: TextStyle(color: Colors.white24),
                  border: OutlineInputBorder(),
                ),
                controller: controller,
                onSubmitted: (text) {
                  client.sendmessage(text);
                  controller.clear();
                  setState(() {});
                },
              ),
            ),
            new Container(
                width: 400,
                height: 75,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RawMaterialButton(
                      elevation: 2,
                      fillColor: Colors.red,
                      child: Icon(
                        Icons.wifi_rounded,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(10.0),
                      shape: CircleBorder(),
                      onPressed: () {
                        client.connection();
                      },
                    ),
                    RawMaterialButton(
                      elevation: 2.0,
                      fillColor: Colors.red,
                      child: Icon(
                        Icons.delete_rounded,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.all(10.0),
                      shape: CircleBorder(),
                      onPressed: () {
                        setState(() {
                          Client.messages.clear();
                        });
                      },
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
