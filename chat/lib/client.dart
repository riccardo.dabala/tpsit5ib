import 'dart:io';

class Client {
  String name;
  Socket sck;
  static List<String> messages = [];
  static int nClient = 0;

  Client() {
    nClient++;
    name = '$nClient';
  }

  void dataHandler(data) {
    String s_data = String.fromCharCodes(data).trim();
    messages.add(s_data);
    print(s_data);
  }

  void sendmessage(String s) {
    sck.write(this.name + ': ' + s);
  }

  void errorHandler(error, StackTrace trace) {
    print(error);
  }

  void doneHandler() {
    sck.destroy();
    exit(0);
  }

  void connection() {
    Socket.connect("192.168.137.159", 3000).then((Socket sck) {
      this.sck = sck;
      this.sck.listen(dataHandler,
          onError: errorHandler, onDone: doneHandler, cancelOnError: false);
    }).catchError((Error e) {
      print("Connection failed: $e");
      exit(1);
    });
  }
}
