# Chat

La chat è stata realizzata a partire dal materiale fornito dal prof. Morettin.

Questo è il metodo per la connessione:

### Connessione
<pre><code> 
    void connection() {
        Socket.connect("192.168.137.159", 3000).then((Socket sck) {
            this.sck = sck;
            this.sck.listen(dataHandler,
            onError: errorHandler, onDone: doneHandler, cancelOnError: false);
        }).catchError((Error e) {
            print("Connection failed: $e");
            exit(1);
        });
    }
}</code></pre>

Ho cambiato il colore dello sfondo per trasformare il tema dell'applicazione con questa riga di codice:

### Dark theme
  <pre><code>
    backgroundColor: Color(0xFF202020),
  </code></pre>

Per l'interfaccia ho usato vari elementi, tra cui;

- ListView (per visualizzare i messaggi):

### ListView
  <pre><code>
    new Expanded(
        child: new ListView.builder(
            itemCount: Client.messages.length,
            itemBuilder: (BuildContext ctxt, int Index) {
                Text t = new Text(Client.messages[Index],
                style: TextStyle(color: Colors.white));
                return t;
            }
        )
    ),
  </code></pre>

cambiando il colore del testo con la seguente riga

<pre><code>
    style: TextStyle(color: Colors.white));
</code></pre>

- TextField (per l'inserimento di un messaggio):

### TextField
  <pre><code>
    child: TextField(
            style: TextStyle(fontSize: 20.0, height: 1.3, color: Colors.white),
            textAlign: TextAlign.center,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration.collapsed(
                hintText: 'Write a message...',
                hintStyle: TextStyle(color: Colors.white24),
                border: OutlineInputBorder(),
            ),
            controller: controller,
            onSubmitted: (text) {
                client.sendmessage(text);
                controller.clear();
                setState(() {});
            },
    ),
  </code></pre>

- RawMaterialButton (uno per la connessione e uno per cancellare tutta la chat):

### Connection RawMaterialButton
  <pre><code>
    RawMaterialButton(
        elevation: 2,
        fillColor: Colors.red,
        child: Icon(
            Icons.wifi_rounded,
            size: 30.0,
            color: Colors.white,
        ),
        padding: EdgeInsets.all(10.0),
        shape: CircleBorder(),
        onPressed: () {
            client.connection();
        },
    ),
  </code></pre>

### Clear RawMaterialButton
  <pre><code>
    RawMaterialButton(
        elevation: 2.0,
        fillColor: Colors.red,
        child: Icon(
            Icons.delete_rounded,
            size: 30.0,
            color: Colors.white,
        ),
        padding: EdgeInsets.all(10.0),
        shape: CircleBorder(),
        onPressed: () {
            setState(() {
                Client.messages.clear();
            });
        },
    ),
  </code></pre>