import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _hours, _minutes, _seconds;
  String _time = '00:00:00';
  Stream<int> _ticker;
  StreamSubscription _tickerSubscription;
  AnimationController _playPauseController;

  @override
  void initState() {
    super.initState();
    _hours = _minutes = _seconds = 0;
    _ticker = Stream.periodic(Duration(seconds: 1), (callBack) => callBack);

    _tickerSubscription = _ticker.listen((event) {
      if (_seconds == 59) {
        if (_minutes == 59) {
          setState(() {
            _seconds = _minutes = 0;
            _hours++;
          });
        } else {
          setState(() {
            _seconds = 0;
            _minutes++;
          });
        }
      } else {
        setState(() {
          _seconds++;
        });
      }
      setState(() {
        _time = getCurrentTime();
      });
    });
    _tickerSubscription.pause();

    _playPauseController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  }

  void onPlayPausePressed() {
    if (_tickerSubscription.isPaused) {
      setState(() {
        _tickerSubscription.resume();
        _playPauseController.forward();
      });
    } else {
      setState(() {
        _tickerSubscription.pause();
        _playPauseController.reverse();
      });
    }
  }

  String getCurrentTime() {
    String hours, minutes, seconds;

    hours = _hours < 10 ? '0$_hours' : '$_hours';
    minutes = _minutes < 10 ? '0$_minutes' : '$_minutes';
    seconds = _seconds < 10 ? '0$_seconds' : '$_seconds';

    return "$hours:$minutes:$seconds";
  }

  void onStopPressed() {
    //if (_tickerSubscription.isPaused) {
    setState(() {
      _hours = _minutes = _seconds = 0;
      _time = '00:00:00';
    });
    //}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cronometro', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Color(0xFF202020),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                _time,
                style: TextStyle(color: Colors.white, fontSize: 75.0, shadows: [
                  Shadow(
                    color: Colors.white38,
                    blurRadius: 0.0,
                    offset: Offset(2, 2),
                  )
                ]),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RawMaterialButton(
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: AnimatedIcon(
                      color: Colors.black,
                      progress: _playPauseController,
                      icon: AnimatedIcons.play_pause,
                      size: 50.0,
                    ),
                    padding: EdgeInsets.all(10.0),
                    shape: CircleBorder(),
                    onPressed: () => onPlayPausePressed(),
                  ),
                  RawMaterialButton(
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Icon(
                      Icons.stop,
                      size: 50.0,
                      color: Colors.black,
                    ),
                    padding: EdgeInsets.all(10.0),
                    shape: CircleBorder(),
                    onPressed: () => onStopPressed(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
