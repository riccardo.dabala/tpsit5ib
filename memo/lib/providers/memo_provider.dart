import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MemoProvider {
  static Database db;

  static Future open() async {
    db = await openDatabase(join(await getDatabasesPath(), 'memo.db'),
        version: 1, onCreate: (Database db, int version) async {
      db.execute('''
          create Table Memos(
            id integer primary key autoincrement,
            title text not null,
            text text not null
          )
        ''');
    });
  }

  static Future<List<Map<String, dynamic>>> getMemoList() async {
    if (db == null) {
      await open();
    }
    return await db.query('Memos');
  }

  static Future insertMemo(Map<String, dynamic> memo) async {
    await db.insert('Memos', memo);
  }

  static Future updateMemo(Map<String, dynamic> memo) async {
    await db.update(
      'Memos',
      memo,
      where: 'id = ?',
      whereArgs: [memo['id']],
    );
  }

  static Future deleteMemo(int id) async {
    await db.delete(
      'Memos',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
