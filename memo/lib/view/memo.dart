import 'package:flutter/material.dart';
import 'package:memo/providers/memo_provider.dart';
import 'package:memo/widgets/memo_widget.dart';
import 'package:flutter/cupertino.dart';

enum MemoMode { Editing, Adding }

class Memo extends StatefulWidget {
  final MemoMode memoMode;
  final Map<String, dynamic> memo;
  //final int index;

  Memo(this.memoMode, this.memo);

  @override
  MemoState createState() {
    return new MemoState();
  }
}

class MemoState extends State<Memo> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _textController = TextEditingController();

  List<Map<String, String>> get _memos => MemoWidget.of(context).memos;

  @override
  void didChangeDependencies() {
    if (widget.memoMode == MemoMode.Editing) {
      _titleController.text = widget.memo['title'];
      _textController.text = widget.memo['text'];
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF202020),
      appBar: AppBar(
        title:
            Text(widget.memoMode == MemoMode.Adding ? 'Add Memo' : 'Edit Memo'),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                  hintText: 'Memo title',
                  hintStyle: TextStyle(color: Colors.white30)),
              style: TextStyle(color: Colors.white),
            ),
            Container(
              height: 5.0,
            ),
            TextField(
              maxLines: 5,
              textInputAction: TextInputAction.newline,
              controller: _textController,
              decoration: InputDecoration(
                  hintText: 'Memo text',
                  hintStyle: TextStyle(color: Colors.white30)),
              style: TextStyle(color: Colors.white),
            ),
            Container(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _MemoButton(Colors.green, Icon(Icons.save), () {
                  final title = _titleController.text;
                  final text = _textController.text;

                  if (widget?.memoMode == MemoMode.Adding) {
                    //_memos.add({'title': title, 'text': text});
                    MemoProvider.insertMemo({'title': title, 'text': text});
                  } else if (widget?.memoMode == MemoMode.Editing) {
                    //_memos[widget.index] = {'title': this._titleController.text, 'text': this._textController.text};
                    MemoProvider.updateMemo({
                      'id': widget.memo['id'],
                      'title': _titleController.text,
                      'text': _textController.text,
                    });
                  }
                  Navigator.pop(context);
                }),
                /*Container(
                  height: 16.0,
                ),*/
                _MemoButton(Colors.black, Icon(Icons.cancel_rounded), () {
                  Navigator.pop(context);
                }),
                /*widget.memoMode == MemoMode.Editing
                    ? Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: */
                _MemoButton(Colors.red, Icon(Icons.delete_rounded), () async {
                  //_memos.removeAt(widget.index);
                  await MemoProvider.deleteMemo(widget.memo['id']);
                  Navigator.pop(context);
                }),
                /*)
                    : Container()*/
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _MemoButton extends StatelessWidget {
  final Color _color;
  final Icon icon;
  final Function _onPressed;

  _MemoButton(this._color, this.icon, this._onPressed);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: _onPressed,
      child: this.icon,
      backgroundColor: _color,
      /*height: 40,
      minWidth: 100,*/
    );
  }
}
