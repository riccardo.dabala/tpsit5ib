import 'package:flutter/material.dart';
import 'package:memo/providers/memo_provider.dart';
import 'package:flutter/cupertino.dart';
//import 'package:memo/widgets/memo_widget.dart';
import 'memo.dart';

class MemoList extends StatefulWidget {
  @override
  MemoListState createState() {
    return new MemoListState();
  }
}

class MemoListState extends State<MemoList> {
  //List<Map<String, String>> get _memos => MemoWidget.of(context).memos;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF202020),
      appBar: AppBar(
        title: Text('Memos'),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: MemoProvider.getMemoList(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              final memos = snapshot.data;
              return ListView.builder(
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Memo(MemoMode.Editing, memos[index]))).then((value) => setState(() {}));
                    },
                    child: Card(
                      color: Color(0xFFFFFF99),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, bottom: 30, left: 13.0, right: 22.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            _MemoTitle(memos[index]['title']),
                            Container(
                              height: 4,
                            ),
                            _MemoText(memos[index]['text'])
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: memos.length,
              );
            }
            return Center(
              child: CircularProgressIndicator(backgroundColor: Colors.black),
            );
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Memo(MemoMode.Adding, null))).then((value) => setState(() {}));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class _MemoTitle extends StatelessWidget {
  final String _title;

  _MemoTitle(this._title);

  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
    );
  }
}

class _MemoText extends StatelessWidget {
  final String _text;

  _MemoText(this._text);

  @override
  Widget build(BuildContext context) {
    return Text(
      _text,
      style: TextStyle(color: Colors.grey.shade600),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }
}
