import 'package:flutter/material.dart';
import 'package:memo/view/memo_list.dart';
import 'package:memo/widgets/memo_widget.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MemoWidget(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Memo',
        home: MemoList(),
      ),
    );
  }
}
