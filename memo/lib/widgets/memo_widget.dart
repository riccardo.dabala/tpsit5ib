import 'package:flutter/material.dart';

class MemoWidget extends InheritedWidget {
  final memos = [
    {'title': 't1', 'text': 'txt1'},
    {'title': 't2', 'text': 'txt2'},
    {'title': 't3', 'text': 'txt3'},
    {'title': 't4', 'text': 'txt4'}
  ];

  MemoWidget(Widget child) : super(child: child);

  static MemoWidget of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MemoWidget>();
  }

  @override
  bool updateShouldNotify(MemoWidget oldWidget) {
    return oldWidget.memos != memos;
  }
}
