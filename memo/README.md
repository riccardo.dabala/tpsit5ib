# Memo

Quest'applicazione permette di salvare i propri memo.

Questo è il metodo per la creazione del database:

### Creazione database
<pre><code> 
    static Future open() async {
        db = await openDatabase(join(await getDatabasesPath(), 'memo.db'),
            version: 1, onCreate: (Database db, int version) async {
                db.execute('''
                create Table Memos(
                    id integer primary key autoincrement,
                    title text not null,
                    text text not null
                )
                ''');
            }
        );
    }
</code></pre>

Ho cambiato il colore dello sfondo per trasformare il tema dell'applicazione con questa riga di codice:

### Dark theme
  <pre><code>
    backgroundColor: Color(0xFF202020),
  </code></pre>

Per l'interfaccia della schermata iniziale ho utilizzato:

- ListView (per visualizzare i vari memo):

### ListView
  <pre><code>
    return ListView.builder(
        itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Memo(MemoMode.Editing, memos[index])
                        )
                    ).then((value) => setState(() {}));
                },
                child: Card(
                    color: Color(0xFFFFFF99),
                    child: Padding(
                    padding: const EdgeInsets.only(top: 30.0, bottom: 30, left: 13.0, right: 22.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                            _MemoTitle(memos[index]['title']),
                            Container(
                                height: 4,
                            ),
                            _MemoText(memos[index]['text'])
                        ],
                    ),
                    ),
                ),
            );
        },
        itemCount: memos.length,
    );
  </code></pre>

- FloatingActionButton (per aggiungere i memo):

### FloatingActionButton
  <pre><code>
    floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Memo(MemoMode.Adding, null))).then((value) => setState(() {}));
        },
        child: Icon(Icons.add),
    ),
  </code></pre>

- Due widget per la gestione di titolo e testo dei memo

### MemoTitle
  <pre><code>
  class _MemoTitle extends StatelessWidget {
    final String _title;

    _MemoTitle(this._title);

    @override
    Widget build(BuildContext context) {
        return Text(
            _title,
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        );
    }
  }
  </code></pre>

### MemoText
  <pre><code>
  class _MemoText extends StatelessWidget {
    final String _text;

    _MemoText(this._text);

    @override
    Widget build(BuildContext context) {
        return Text(
            _text,
            style: TextStyle(color: Colors.grey.shade600),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
        );
    }
  }
  </code></pre>

Per l'interfaccia di modifica o aggiunta dei memo ho utilizzato:

- Un controller per il titolo e uno per il testo

### Controller
<pre><code>
    final TextEditingController _titleController = TextEditingController();
    final TextEditingController _textController = TextEditingController();
</code></pre>

- Un metodo per controllare se il memo è nuovo oppure è da modificare

### didChangeDependencies() 
<pre><code>
    void didChangeDependencies() {
        if (widget.memoMode == MemoMode.Editing) {
        _titleController.text = widget.memo['title'];
        _textController.text = widget.memo['text'];
        }
        super.didChangeDependencies();
    }
</code></pre>

- due TextField per il titolo e il testo del memo

### TextField Titolo
<pre><code>
    TextField(
        controller: _titleController,
        decoration: InputDecoration(
            hintText: 'Memo title',
            hintStyle: TextStyle(color: Colors.white30)
        ),
        style: TextStyle(color: Colors.white),
    ),
</code></pre>

### TextField Testo
<pre><code>
    TextField(
        maxLines: 5,
        textInputAction: TextInputAction.newline,
        controller: _textController,
        decoration: InputDecoration(
            hintText: 'Memo text',
            hintStyle: TextStyle(color: Colors.white30)
        ),
        style: TextStyle(color: Colors.white),
    ),
</code></pre>

- Un widget MemoButton per gestire i bottoni

### MemoButton
<pre><code>
    class _MemoButton extends StatelessWidget {
        final Color _color;
        final Icon icon;
        final Function _onPressed;

        _MemoButton(this._color, this.icon, this._onPressed);

        @override
        Widget build(BuildContext context) {
            return FloatingActionButton(
                onPressed: _onPressed,
                child: this.icon,
                backgroundColor: _color,
            );
        }
    }
</code></pre>

- Una Row per contenere i MemoButton

- I MemoButton per il salvataggio, l'annullamento o la cancellazione

### MemoButton Save
<pre><code>
    _MemoButton(Colors.green, Icon(Icons.save), () {
        final title = _titleController.text;
        final text = _textController.text;

        if (widget?.memoMode == MemoMode.Adding) {
            MemoProvider.insertMemo({'title': title, 'text': text});
        } else if (widget?.memoMode == MemoMode.Editing) {
            MemoProvider.updateMemo({
                'id': widget.memo['id'],
                'title': _titleController.text,
                'text': _textController.text,
            });
        }
        Navigator.pop(context);
    }),
</code></pre>

L'if controlla se è un nuovo memo da aggiungere o già esistente e da modificare, e si comporta di conseguenza

### MemoButton Cancel
<pre><code>
    _MemoButton(Colors.black, Icon(Icons.cancel_rounded), () {
        Navigator.pop(context);
    }),
</code></pre>

Torna semplicemente alla pagina precedente

### MemoButton Delete
<pre><code>
    _MemoButton(Colors.red, Icon(Icons.delete_rounded), () async {
        await MemoProvider.deleteMemo(widget.memo['id']);
        Navigator.pop(context);
    }),
</code></pre>
